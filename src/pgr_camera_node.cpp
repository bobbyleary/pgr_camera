#include <ros/ros.h>
#include <stdlib.h>
#include <pgr_camera/Flea3GigE.h>

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

int main(int argc, char** argv)
{
	ros::init(argc, argv, "pgr_camera");
	ros::NodeHandle n;
	Flea3GigE c(n);

	if(c.Start() == 0)
	{
		ROS_INFO("Start successful, entering spin");
		c.Spin();
	}
	return 0;
}