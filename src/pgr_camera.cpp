#include <ros/ros.h>
#include <FlyCapture2.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <pgr_camera/Flea3GigE.h>
#include <image_transport/image_transport.h>
#include <camera_calibration_parsers/parse_ini.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/SetCameraInfo.h>
#include <sensor_msgs/fill_image.h>
#include <cv_bridge/cv_bridge.h>


#include <opencv2/opencv.hpp>

#include <fstream>

#define PGRERROR_OK FlyCapture2::PGRERROR_OK
#define PRINT_ERROR_AND_RETURN_FALSE {ROS_ERROR(error.GetDescription()); return false;}
#define PRINT_ERROR {ROS_ERROR(error.GetDescription());}

Flea3GigE::Flea3GigE(ros::NodeHandle &n) : n_(n), it_(n_)
{
	ros::NodeHandle n_param("~");

	n_param.param<int>("width", this->width, 640);
	n_param.param<int>("height", this->height, 480);
    n_param.param<int>("x_offset", this->x_offset, 320);
    n_param.param<int>("y_offset", this->y_offset, 240);
	// n_param.param<int>("ip_address", this->ip_address, 3232235522); // 3232235522
    n_param.param<double>("frame_rate", this->frame_rate, 15);
	n_param.param<int>("serial", this->serial, 13142472); // 13110655 130.203.223.239
	// nh_param.param<std::string>("subnet", this->subnet, "255.255.255.0");
	// nh_param.param<std::string>("gateway", this->gateway, "0.0.0.0");
    n_param.param<std::string>("intrinsics", this->intrinsics, "/home/david/catkin_ws/src/pgr_camera/intrinsics.yml");
    n_param.param<std::string>("camera_name", this->camera_name, "pgr_camera");
    n_param.param<int>("flip_image", this->flip_image, 2);

    // Trigger Parameters
    n_param.param<bool>("trigger_enabled", this->trigger_enabled, false);
    n_param.param<int>("trigger_polarity", this->trigger_polarity, 0);
    n_param.param<int>("trigger_source", this->trigger_source, 0);
    n_param.param<int>("trigger_mode", this->trigger_mode, 0);

    this->image_pub_ = this->it_.advertiseCamera(std::string(this->camera_name) + "/image_raw",1);
}

Flea3GigE::~Flea3GigE()
{

}


int Flea3GigE::Connect(FlyCapture2::PGRGuid guid)
{

	ROS_INFO("Connecting to camera...\n");

	// Connect to a camera
	error_ = cam_.Connect(&guid);
    if(error_ != PGRERROR_OK){
        PrintError(error_);
    }

	// Get the current image settings
	FlyCapture2::GigEImageSettingsInfo imageSettingsInfo;
    error_ = cam_.GetGigEImageSettingsInfo(&imageSettingsInfo);
    if(error_ != PGRERROR_OK){
        PrintError(error_);
    }

    // Set up the current image settings
    FlyCapture2::GigEImageSettings imageSettings;
    imageSettings.offsetX = x_offset;
    imageSettings.offsetY = y_offset;
    imageSettings.height = this->height; // imageSettingsInfo.maxHeight;
    imageSettings.width = this->width; //imageSettingsInfo.maxWidth;
    imageSettings.pixelFormat = FlyCapture2::PIXEL_FORMAT_422YUV8; // Flycapture2::PIXEL_FORMAT_RAW8 422YUV8

    if(this->trigger_enabled == true){
        Flea3GigE::SetTrigger(this->trigger_polarity, this->trigger_source, this->trigger_mode);
    } else {
        // Set the frame rate
        Flea3GigE::SetFrameRate(this->frame_rate);
    }

    // Set the new settings
    ROS_INFO("Setting GigE image settings...\n");
    error_ = cam_.SetGigEImageSettings(&imageSettings);
    if(error_ != PGRERROR_OK){
        PrintError(error_);
    }

    // Set up the ROS image message with the camera information
    image_.height = this->height;
    image_.width = this->width;
    image_.step = 3 * this->width;
    image_.encoding = sensor_msgs::image_encodings::RGB8;
    image_.header.frame_id = this->camera_name;
    image_.data.resize(image_.step * image_.height);

    Flea3GigE::LoadIntrinsics(this->intrinsics, this->camera_name);

    // Flea3GigE::SetGain(false, 0.0);

    // Flea3GigE::SetShutter(true);

    // Flea3GigE::SetFrameRate();

    // Flea3GigE::SetExposure();

    // FlyCapture2::TriggerMode triggerMode;
    // error_ = cam_.GetTriggerMode(&triggerMode);

    // // Set camera to trigger mode 0
    // triggerMode.onOff = false;

    // error_ = cam_.SetTriggerMode(&triggerMode);

    // Flea3GigE::SetExposure(false, true, 50);

    // FlyCapture2::TriggerMode triggerMode;
    // error_ = cam_.GetTriggerMode(&triggerMode);
    // triggerMode.onOff = false;
    // error_ = cam_.SetTriggerMode(&triggerMode);

    // FlyCapture2::FC2Config fc2Config;
    // error_ = cam_.GetConfiguration(&fc2Config);
    // fc2Config.grabMode = FlyCapture2::DROP_FRAMES;
    // fc2Config.isochBusSpeed = FlyCapture2::BUSSPEED_S_FASTEST;
    // fc2Config.asyncBusSpeed = FlyCapture2::BUSSPEED_S_FASTEST;
    // fc2Config.highPerformanceRetrieveBuffer = true;
    // error_ = cam_.SetConfiguration(&fc2Config);

    // FlyCapture2::Mode mode;
    // error_ = cam_.GetGigEImagingMode(&mode);
    // ROS_INFO("%i", mode);

    // FlyCapture2::CameraInfo info;
    // error_ = cam_.GetCameraInfo(&info);
    // ROS_INFO("%i", info.driverType);

    // Start capturing images
    ROS_INFO("Starting image capture...");
    error_ = cam_.StartCapture();
    if(error_ != PGRERROR_OK){
        PrintError(error_);
    }

	return 0;
}

int Flea3GigE::GrabImage()
{

    // Retrieve an image
    error_ = cam_.RetrieveBuffer(&rawImage_);
    if(error_ != PGRERROR_OK){
        PrintError(error_);
    }

    // Convert the raw image to 8-bit RGB
    error_ = rawImage_.Convert( FlyCapture2::PIXEL_FORMAT_RGB8, &convertedImage_ );

    // Set the ROS time when the new image comes in.
    image_.header.stamp = ros::Time::now();

    // Set the ROS time of the new image
    image_.header.stamp = ros::Time::now();

    // If we need to flip the image, use the OpenCV command to flip the image.
    // Unfortunately this means we need to convert it to a cv::Mat first.
    // To avoid the image being flip by default, the default ros_param above is set to 2
    // 0 : flip around x-axis
    // positive (e.g. 1) : flip around y-axis
    // negative (e.g. -1) : flip around both 
    if(this->flip_image == 0 || this->flip_image == 1 || this->flip_image == -1){
        cv::Mat src_img(convertedImage_.GetCols(),convertedImage_.GetRows(),CV_8UC3,convertedImage_.GetData());
        cv::Mat flip_img(convertedImage_.GetCols(),convertedImage_.GetRows(),CV_8UC3);
        cv::flip(src_img,flip_img,this->flip_image);    
        sensor_msgs::fillImage(image_, image_.encoding, image_.height, image_.width, image_.width*3, flip_img.data);
    } else {
        sensor_msgs::fillImage(image_, image_.encoding, image_.height, image_.width, image_.width*3, convertedImage_.GetData());
    }

    this->image_pub_.publish(image_, cam_info_, ros::Time::now());

    return 0;
}

void Flea3GigE::LoadIntrinsics(std::string file_name, std::string camera_name)
{
    std::fstream fin(file_name.c_str());
    if(fin.is_open()){
        fin.close();
        if(camera_calibration_parsers::readCalibrationIni(file_name, camera_name, cam_info_)){
            cam_info_.header.frame_id = this->camera_name;
            ROS_INFO("Loaded calibration for camera '%s'", camera_name.c_str());
        } else {
            ROS_WARN("Failed to load intrinsics from camera");
        }
    } else {
        ROS_WARN("Intrinsics file not found: %s", file_name.c_str());
    }
}

void Flea3GigE::SetTrigger(int trigger_polarity, int trigger_source, int trigger_mode)
{
    FlyCapture2::TriggerMode triggerMode;

    // Enable trigger
    triggerMode.onOff = true;

    triggerMode.polarity = trigger_polarity;

    triggerMode.source = trigger_source;

    triggerMode.mode = trigger_mode;

    error_ = cam_.SetTriggerMode(&triggerMode);

    if(error_ != PGRERROR_OK){
        PrintError(error_);
    }

}

void Flea3GigE::SetShutter(bool _auto)
{
    FlyCapture2::Property prop;

    prop.type = FlyCapture2::SHUTTER;

    error_ = cam_.GetProperty( &prop );
    ROS_INFO("%i, %i, %i, %f", prop.autoManualMode, prop.onOff, prop.absControl, prop.absValue);

    prop.type = FlyCapture2::FRAME_RATE;
    error_ = cam_.GetProperty( &prop );
    ROS_INFO("%i, %i, %i, %f", prop.autoManualMode, prop.onOff, prop.absControl, prop.absValue);

    prop.type = FlyCapture2::AUTO_EXPOSURE;
    error_ = cam_.GetProperty( &prop );
    ROS_INFO("%i, %i, %i, %f", prop.autoManualMode, prop.onOff, prop.absControl, prop.absValue);

    prop.type = FlyCapture2::TRIGGER_MODE;
    error_ = cam_.GetProperty( &prop );
    ROS_INFO("%i, %i, %i, %f", prop.autoManualMode, prop.onOff, prop.absControl, prop.absValue);

    prop.type = FlyCapture2::TRIGGER_DELAY;
    error_ = cam_.GetProperty( &prop );
    ROS_INFO("%i, %i, %i, %f", prop.autoManualMode, prop.onOff, prop.absControl, prop.absValue);

    prop.autoManualMode = true;
    prop.onOff = true;
    prop.absControl = true;
    // prop.absValue = 31.588297;

    error_ = cam_.SetProperty(&prop);
}

void Flea3GigE::SetExposure()
{
    FlyCapture2::Property prop;

    prop.type = FlyCapture2::AUTO_EXPOSURE;
    prop.autoManualMode = true;
    prop.onOff = true;
    prop.absControl = true;

    error_ = cam_.SetProperty(&prop);
}

void Flea3GigE::SetFrameRate(double frame_rate)
{
    FlyCapture2::Property prop;

    prop.type = FlyCapture2::FRAME_RATE;
    prop.autoManualMode = false;
    prop.onOff = true;
    prop.absControl = true;
    prop.absValue = frame_rate;

    error_ = cam_.SetProperty(&prop);

}

void Flea3GigE::SetGain(bool _auto, float value = 0.0)
{

}

// int Flea3GigE::Stop()
// {
// 	Error_ error_;
// 	GigECamera cam;

// 	error_ = cam.StopCapture();
//     if(error__ != PGRERROR__OK)
//     {
//         ROS_ERROR_(error_);
//         ros::shutdown();
//     }      

//     // Disconnect the camera
//     error_ = cam.Disconnect();
//     if(error__ != PGRERROR__OK)
//     {
//         ROS_ERROR_(error_);
//         ros::shutdown();
//     }

//     return 0;
// }

int Flea3GigE::Spin()
{
	while(this->n_.ok())
	{
		GrabImage();
		ros::spinOnce();
	}
	return 0;
}

int Flea3GigE::Start()
{  

    FlyCapture2::BusManager busMgr;

    // error_ = busMgr.ForceIPAddressToCamera(static_cast<FlyCapture2::MACAddress>(this->mac),
    // 									  static_cast<FlyCapture2::IPAddress>(this->ip),
    // 									  static_cast<FlyCapture2::IPAddress>(this->subnet),
    // 									  static_cast<FlyCapture2::IPAddress>(this->gateway));


    // Get the camera from the specified IP Address. This can be set as an argument to the node. (Doesn't work yet.)
    FlyCapture2::PGRGuid guid;
    // error_ = busMgr.GetCameraFromIPAddress(static_cast<FlyCapture2::IPAddress>(this->ip_address), &guid);
    error_ = busMgr.GetCameraFromSerialNumber(this->serial, &guid);
    if(error_ != PGRERROR_OK){
        PrintError(error_);
    }

    // Get the specified cameras interface type
    FlyCapture2::InterfaceType interfaceType;
    error_ = busMgr.GetInterfaceTypeFromGuid( &guid, &interfaceType );
    if(error_ != PGRERROR_OK){
        PrintError(error_);
    }

    if ( interfaceType == FlyCapture2::INTERFACE_GIGE )
    {
    	// Connect to the camera!
        Connect(guid);
      	ROS_INFO("Connected!");
    }

    return 0;
}

void Flea3GigE::PrintError( FlyCapture2::Error error )
{
    error.PrintErrorTrace();
}