#include <ros/ros.h>
#include <FlyCapture2.h>
#include <sensor_msgs/Image.h>
#include <image_transport/image_transport.h>

#include <opencv2/opencv.hpp>

class Flea3GigE
{
	protected:
		ros::NodeHandle n_;
		image_transport::ImageTransport it_;
		
	public:
		
		image_transport::CameraPublisher image_pub_;

		FlyCapture2::GigECamera cam_;
		sensor_msgs::Image image_;
		sensor_msgs::CameraInfo cam_info_;
		FlyCapture2::Image rawImage_;  
    	FlyCapture2::Image convertedImage_;
    	FlyCapture2::Image copy_;

    	FlyCapture2::Error error_;

    	std::string intrinsics;
    	int width;
    	int height;
    	int x_offset;
    	int y_offset;
    	int serial;
    	double frame_rate;
    	std::string camera_name;
    	int flip_image;
    	bool trigger_enabled;
    	int trigger_source;
    	int trigger_polarity;
    	int trigger_mode;

		
		Flea3GigE(ros::NodeHandle &n);

		~Flea3GigE();

		int Start();

		int Spin();

		int GrabImage();

		int Connect(FlyCapture2::PGRGuid guid);

		void LoadIntrinsics(std::string file_name, std::string camera_name);

		void SetTrigger(int trigger_polarity, int trigger_source, int trigger_mode);

		void SetShutter(bool _auto);

		void SetExposure();

		void SetGain(bool _auto, float value);

		void SetFrameRate(double frame_rate);

		void PrintError(FlyCapture2::Error error);

		cv::Mat rotateImage(const cv::Mat source, double angle);


};